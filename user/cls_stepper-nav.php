<?php include 'cls_head.php'; ?>

<div class="stepper-nav">
    <button class="step btn btn-success btn-lg btn-flat step-prev" data-direction="prev" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Back</font></font></button>
    <button class="step btn btn-success btn-lg btn-flat step-next" data-direction="next" type="button"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">global.next</font></font></button>
  
  
    <h3 class="step-title" data-hide-on-step="2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Create a new feedback widget</font></font></h3>
  
    <ul class="stepper-steps">
        <li class="active steps">
            <a href="step1" data-step="1" class="link"> 
                    <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot active">1</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">URL address
                    </font></font>
                </span>
            </a>
        </li>

        <li class="steps">
            <a href="step2" data-step="2" class="link">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">2</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Emails after purchase
                    </font></font>
                </span>
            </a>
        </li>

        <li class="steps">
            <a href="step3" data-step="3" class="link">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">3</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Widget on the site
                    </font></font>
                </span>
            </a>
        </li>

        <li class="steps">
            <a href="step4" data-step="4" class="link">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">4</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Questions for the client
                    </font></font>
                </span>
            </a>
        </li>

        <li class="steps">
            <a href="step5" data-step="5" class="link">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">5</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">Moderation settings
                    </font></font>
                </span>
            </a>
        </li>

        <li class="steps">
            <a href="step6" data-step="6" class="link">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">6</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">statute
                    </font></font>
                </span>
            </a>
        </li>
        <li class="steps">
            <a href="step7" data-step="7" class="link">
                <span class="step-nmbr">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">
                        <span class="dot">7</span>
                    </font></font>
                </span> 
                <span class="text">
                    <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;"> Reward for opinions 
                    </font></font>
                </span>
            </a>
        </li>
    </ul>
  
</div>